import React, {Component} from 'react';

export default class Nav extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoggedIn: true
        }
    }

    componentDidMount(){
        if(localStorage.getItem("uid") === null){
            this.setState({isLoggedIn: false});
        }
    }

    onClick(index){
        this.props.onNavSelect(index);
    }

    render(){
        return(
            <div id="nav">
                <div id="nav-top">
                    <img src={require("../../images/icon.png")} id="img-logo"/>
                    <h2 className="title1">Live Football Stats</h2>
                </div>
                <div id="nav-mid">
                    <ul>
                        <li onClick={this.onClick.bind(this, 1)}>Home</li>
                        <li onClick={this.onClick.bind(this, 2)} className={this.state.isLoggedIn ? "shown" : "hidden"}>Notification</li>
                        <li onClick={this.onClick.bind(this, 3)}>Country List</li>
                        <li onClick={this.onClick.bind(this, 4)}>Leagues</li>
                        <li onClick={this.onClick.bind(this, 5)}>Upcoming Match</li>
                        <li onClick={this.onClick.bind(this, 6)} className={this.state.isLoggedIn ? "hidden" : "shown"}>Login</li>
                        <li onClick={this.onClick.bind(this, 7)} className={this.state.isLoggedIn ? "hidden" : "shown"}>Sign Up</li>
                        <li onClick={this.onClick.bind(this, 8)} className={this.state.isLoggedIn ? "shown" : "hidden"}>Past Matches</li>
                        <li onClick={this.onClick.bind(this, 9)} className={this.state.isLoggedIn ? "shown" : "hidden"}>Chat Room</li>
                        <li onClick={this.onClick.bind(this, 10)} className={this.state.isLoggedIn ? "shown" : "hidden"}>Logout</li>
                    </ul>
                </div>
                <div id="nav-bot">
                    <label>Powered by Live Football Stats</label>
                </div>
            </div>
        )
    }
}