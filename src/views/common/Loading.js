import React from 'react';

export default class Loading extends React.Component {
    render() {
        return (
            <div id="lfs-loading-container"
                className={this.props.isLoading ? 'shown' : 'hidden'}>
                <img id="lfs-loading-gif"
                    src={require("../../images/loading.gif")} />
            </div>
        )
    }
}