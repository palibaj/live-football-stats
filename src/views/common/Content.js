import React, { Component } from 'react';
import HomeLoginContent from '../contents/HomeLoginContent';
import Register from '../auth/Register';
import Login from '../auth/Login';
import CountryList from '../contents/CountryList';
import ResetPassword from '../auth/ResetPassword';
import UpComingMatch from '../contents/UpComingMatch';
import LeagueList from '../contents/LeagueList';
import PastMatch from '../contents/PastMatch';
import Logout from '../contents/Logout';
import Chatroom from '../contents/Chatroom';

export default class Content extends Component {
    render() {
        return (
            <div id="content">
             {(() => {
                    switch (this.props.index) {
                        case 1: return <HomeLoginContent/>;
                        case 2: return "Coming Soon...";
                        case 3: return <CountryList/>;
                        case 4: return <LeagueList/>;
                        case 5: return <UpComingMatch/>;
                        case 6: return <Login/>;
                        case 7: return <Register/>
                        case 8: return <PastMatch/>;
                        case 9: return <Chatroom/>;
                        case 10: return <Logout/>;
                    }
                })()}
            </div>
        )
    }
}