import React from 'react';
import axios from 'axios';
import Loading from '../common/Loading';

export default class LeagueList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true
        }
    }
    componentDidMount() {
        const url = "https://live-football-stats.herokuapp.com/?leagues";
        axios.get(url).then(response => {
            this.setState({ data: response.data.data.leagues,
            isLoading: false });        
        });
    }

    render() {
        return (
            <div>
                <h2 className="title1">League List</h2>                
                {
                    this.state.data.map((item) =>
                    <label className="btn-shadow lbl-country-list">{item.league_name}<sub><b className="league-contry-name">{item.country_name}</b></sub></label>
                    )
                }
                <Loading isLoading={this.state.isLoading} />
            </div>
        )
    }
}