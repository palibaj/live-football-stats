import React from 'react';
import axios from 'axios';
import Loading from '../common/Loading';
import Match from '../components/Match';

export default class UpComingMatch extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data: [],
            isLoading: true
        }

    }
    componentDidMount(){
        const url = "https://live-football-stats.herokuapp.com/?fixture-matches";
        axios.get(url).then(response => {            
            console.log(response.data.data.fixtures);
            this.setState({isLoading: false, data: response.data.data.fixtures});
        });
    }
    render(){
        return(
            <div>
                <h2 className="title1">Upcoming Match</h2>
                {
                    this.state.data.map((item) => {
                        return <Match time={item.time}
                        date={item.date}
                        team={item.home_name + " Vs " + item.away_name}/>
                    })
                }
                <Loading isLoading={this.state.isLoading}/>
            </div>
        )
    }
}