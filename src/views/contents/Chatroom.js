import React from 'react';
import fire from '../../config/Fire';

export default class Chatroom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageText: "",
            data: []
        }
    }
    componentDidMount(){
        fire.database().ref("Chatroom").on("child_added", (data) => {
            console.log(data.val()); 
            let dat = this.state.data;
            dat.push(data.val());
            this.setState({data: dat});            
        });
    }
    onSend(e) {
        if (e.keyCode == 13) {
            if(this.state.messageText === ""){
                return;
            }
            const data = {
                type: "OWN",
                message: this.state.messageText,
                sender: localStorage.getItem("fullName")
            }
            fire.database().ref('Chatroom').push(data);
            this.setState({messageText: ""});
        }
    }
    render() {
        return (
            <div>
                <div id="chatbox">
                {
                    Object.keys(this.state.data).map((i) => {
                        const item = this.state.data[i];
                        if(item.type == "User"){
                            return <div className="msg-user"><b>{item.sender}</b> : <span>{item.message}</span></div>;
                        }else{
                            return <div className="msg-own">{item.message}</div>;
                        }
                    })
                }                                        
                </div>
                <input
                    type="text"
                    maxLength="200"
                    value={this.state.messageText}
                    onChange={e => { this.setState({ messageText: e.target.value }) }}
                    className="chat-input"
                    placeholder="Type your message and press enter to send"
                    onKeyDown={this.onSend.bind(this)} />
            </div>
        )
    }
}