import React from 'react';

export default class HomeLoginContent extends React.Component {
    render() {
        return (
            <div id="home-login-content">
                <img src={require("../../images/slider1.jpg")} id="img-home-login" />
                <label className="title1">We provide live scores for</label><br/>
                <div className="football-league-box">
                    <label>German Football</label>
                </div>
                <div className="football-league-box">
                    <label>UK Football</label>
                </div>
                <div className="football-league-box">
                    <label>US Soccor</label>
                </div>
                <div className="football-league-box">
                    <label>French Football</label>
                </div>
                <div className="football-league-box">
                    <label>Spanish Football</label>
                </div>
                <div className="football-league-box">
                    <label>International Tournaments</label>
                </div>
                <div className="football-league-box">
                    <label>Club Tournaments</label>
                </div>
                <div className="football-league-box">
                    <label>Italian Football</label>
                </div>
                <div className="lts-history-match">
                    
                </div>
            </div>
        )
    }
}