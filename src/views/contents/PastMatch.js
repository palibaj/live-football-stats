import React from 'react';
import Loading from '../common/Loading';
import axios from 'axios';
import Match from '../components/Match';

export default class PastMatch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "Choose League",
            data: [],
            historicalData: [],
            isLoading: true
        }
    }

    componentDidMount() {
        const url = "https://live-football-stats.herokuapp.com/?leagues";
        axios.get(url).then(response => {
            this.setState({
                data: response.data.data.leagues,
                isLoading: false
            });
        });
    }

    onClick(item) {
        this.setState({ isLoading: true });
        const url = "https://live-football-stats.herokuapp.com/?history=" + item.league_id;
        axios.get(url).then(response => {
            this.setState({
                historicalData: response.data.data.match,
                data: [],
                isLoading: false,
                title: item.league_name + " History Match"
            });
        });
    }

    render() {
        return (
            <div>
                <h2 className="title1">{this.state.title}</h2>
                {
                    this.state.data.map((item) =>
                        <label onClick={this.onClick.bind(this, item)} className="btn-shadow lbl-country-list">{item.league_name}<sub><b className="league-contry-name">{item.country_name}</b></sub></label>
                    )
                }
                {
                    this.state.historicalData.map((item) => {
                        return <Match date={item.date} time={item.et_score} team={item.home_name + " vs " + item.away_name}/>
                    })
                }
                <Loading isLoading={this.state.isLoading} />

            </div>
        )
    }
}