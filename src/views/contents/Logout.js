var logout = () => {
    localStorage.removeItem("uid");
    window.location.reload();
}
export default logout;
