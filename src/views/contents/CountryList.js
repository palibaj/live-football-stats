import React from 'react';
import axios from 'axios';
import Loading from '../common/Loading';

export default class CountryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "Country",
            data: [],
            isLoading: true
        }
    }

    componentDidMount() {
        const url = "https://live-football-stats.herokuapp.com/?country-list";
        axios.get(url).then(response => {
            this.setState({
                data: response.data.data.country,
                isLoading: false
            });
        });
    }

    onClick(item) {
        this.setState({ isLoading: true });
        const url = "https://live-football-stats.herokuapp.com/?country-leagues=" + item.id;
        axios.get(url).then(response => {
            this.setState({
                data: response.data.data.league,
                title: "League of " + item.name,
                isLoading: false
            });
        });
    }

    render() {
        return (
            <div>
                <h2 className="title1">{this.state.title}</h2>
                {
                    this.state.data.map((item) =>
                        <label className="btn-shadow lbl-country-list" onClick={this.onClick.bind(this, item)}>{item.name}</label>
                    )
                }
                <Loading isLoading={this.state.isLoading} />
            </div>
        )
    }
}