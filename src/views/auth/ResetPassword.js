import React from 'react';
import Loading from '../common/Loading';
import fire from '../../config/Fire';

export default class ResetPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            isLoading: false,
            errorMessage: "",
            successMessage: ""
        }
    }

    reset() {
        this.setState({ errorMessage: "" });
        fire.auth().sendPasswordResetEmail(this.state.email).then(() => {
            this.setState({
                successMessage: "Password reset link has been sent.",
                email: ""
            });
        }).catch((e) => {
            this.setState({ errorMessage: e.message });
        })
    }

    render() {
        return (
            <div id="frm-register">
                <h1 className="title1"> Reset Password </h1>
                <label>Email</label><br /><br />
                <input type="text"
                    value={this.state.email}
                    placeholder="Email Address"
                    onChange={e => { this.setState({ email: e.target.value }) }} /><br />
                <label className="error-text">{this.state.errorMessage}</label>
                <label className="success-text">{this.state.successMessage}</label>
                <input type="button"
                    className="btn btn-normal btn-round"
                    value="Login"
                    onClick={this.reset.bind(this)} />
                <Loading isLoading={this.state.isLoading} />
            </div>
        )
    }
}