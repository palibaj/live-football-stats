import React from 'react';
import * as firebase from 'firebase';
import Loading from '../common/Loading';
import fire from '../../config/Fire';

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName: "",
            address: "",
            email: "",
            phoneNo: "",
            password: "",
            confirmPassword: "",
            isLoading: false
        }
    }

    register() {
        if (this.state.fullName === "") {
            alert("Please enter your name.");
            return;
        }
        if (this.state.address === "") {
            alert("please enter yor address.");
            return;
        }
        if (this.state.email === "") {
            alert("please enter your email");
            return;
        }
        if (this.state.phoneNo === "") {
            alert("please enter your phone no");
            return;
        }
        if (this.state.password === "") {
            alert("please enter your password");
            return;
        }
        if (this.state.confirmPassword === "") {
            alert("please enter your password again");
            return;
        }
        this.setState({ isLoading: true });
        fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((user) => {
            this.setState({ isLoading: false });
            const data = {
                email: this.state.email,
                fullName: this.state.fullName,
                phoneNo: this.state.phoneNo,
                address: this.state.address
            }
            fire.database().ref("Users/" + user.user.uid).set(data).then(() => {
                localStorage.setItem("uid", user.user.uid);
                localStorage.setItem("fullName", this.state.fullName);
                window.location.reload();
            });
        })
    }


    render() {
        return (
            <div id="frm-register">
                <h1 className="title1">Sign Up Form </h1>
                <label>Full Name</label><br />
                <input type="text"
                    value={this.state.fullName}
                    placeholder="Full Name"
                    onChange={e => { this.setState({ fullName: e.target.value }) }} /><br /><br />
                <label>Address</label><br />
                <input type="text"
                    value={this.state.address}
                    placeholder="Address"
                    onChange={e => { this.setState({ address: e.target.value }) }} /><br /><br />
                <label>E-mail</label><br />
                <input type="text"
                    value={this.state.email}
                    placeholder="Email Address"
                    onChange={e => { this.setState({ email: e.target.value }) }} /><br /><br />
                <label>Phone Number</label><br />
                <input type="text"
                    value={this.state.phoneNo}
                    placeholder="Phone Number"
                    onChange={e => { this.setState({ phoneNo: e.target.value }) }} /><br /><br />
                <label>Password</label><br />
                <input type="password"
                    value={this.state.password}
                    placeholder="Password"
                    onChange={e => { this.setState({ password: e.target.value }) }} /><br /><br />
                <label>Confirm Password</label><br />
                <input type="password"
                    value={this.state.confirmPassword}
                    placeholder="Confirm Password"
                    onChange={e => { this.setState({ confirmPassword: e.target.value }) }} /><br /><br />
                <input type="button"
                    value="Register"
                    onClick={this.register.bind(this)}
                    className={this.state.isLoading ? 'btn btn-normal btn-round hidden' : 'btn btn-normal btn-round shown'} />

                <Loading isLoading={this.state.isLoading} />
            </div>
        )
    };
}