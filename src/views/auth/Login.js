import React from 'react';
import Loading from '../common/Loading';
import fire from '../../config/Fire';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            isLoading: false,
            errorMessage: ""
        }
    }

    login() {
        if (this.state.email === "") {
            alert("Please enter email address.");
            return;
        }

        if (this.state.password === "") {
            alert("Please enter password.");
            return;
        }

        this.setState({ isLoading: true });
        fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
            localStorage.setItem("uid", u.user.uid)
            fire.database().ref("Users/" + u.user.uid).once("value", (data) => {
                console.log(data.val());
                localStorage.setItem("fullName", data.val().fullName);
                this.setState({
                    isLoading: false
                })
                window.location.reload();
            })
        }).catch((e) => {
            console.log(e);
            this.setState({ isLoading: false, errorMessage: e.message })
        });
    }
    render() {
        return (
            <div id="frm-register">
                <h1 className="title1"> Sign In </h1>
                <label>Email</label><br />
                <input type="text"
                    value={this.state.email}
                    placeholder="Email Address"
                    onChange={e => { this.setState({ email: e.target.value }) }} /><br /><br />
                <label>Password</label><br />
                <input type="password"
                    value={this.state.password}
                    placeholder="Password"
                    onChange={e => { this.setState({ password: e.target.value }) }} /><br />
                <label className="error-text">{this.state.errorMessage}</label>
                <input type="button"
                    className="btn btn-normal btn-round"
                    value="Login"
                    onClick={this.login.bind(this)} />
                <a href="#">Forgot Password ?</a>
                <Loading isLoading={this.state.isLoading} />
            </div>
        )

    };

}