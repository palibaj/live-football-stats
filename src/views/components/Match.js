import React from 'react';

export default class Match extends React.Component {
    render() {
        return (
            <div className="match-details">
                <label className="lbl-match-date">{this.props.date}</label>
                <label className="lbl-match-time">{this.props.time}</label>
                <label className="lbl-match-vs">{this.props.team}</label>
            </div>
        )
    }
}