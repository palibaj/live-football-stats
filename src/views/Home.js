import React, { Component } from 'react';
import Nav from './common/Nav';
import Content from './common/Content';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 1
        }
    }

    onNavSelect(index) {
        this.setState({index})
    }
    render() {
        return (
            <div>
                <Nav onNavSelect={this.onNavSelect.bind(this)} />               
                <Content index={this.state.index}/>
            </div>
        )
    }
}