import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyD45c4gvVPemc9z3ws2VCfsQy2tDVk8rsc",
    authDomain: "live-football-stats-e422e.firebaseapp.com",
    databaseURL: "https://live-football-stats-e422e.firebaseio.com",
    projectId: "live-football-stats-e422e",
    storageBucket: "live-football-stats-e422e.appspot.com",
    messagingSenderId: "951971411729"
};
const fire = firebase.initializeApp(config);
export default fire;